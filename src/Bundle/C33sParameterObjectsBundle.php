<?php

declare(strict_types=1);

namespace C33s\Bundle\ParameterObjectsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class C33sParameterObjectsBundle extends Bundle
{
}
