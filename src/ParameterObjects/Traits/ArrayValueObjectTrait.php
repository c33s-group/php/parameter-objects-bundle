<?php

declare(strict_types=1);

namespace C33s\ParameterObjects\Traits;

trait ArrayValueObjectTrait
{
    /**
     * @var array
     */
    private $value;

    private function __construct(array $value)
    {
        $this->value = $value;
    }

    public static function fromArray(array $value): self
    {
        return new self($value);
    }

    public function value(): array
    {
        return $this->value;
    }
}
