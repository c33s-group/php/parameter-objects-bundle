<?php

declare(strict_types=1);

namespace C33s\ParameterObjects\Traits;

trait BooleanValueObjectTrait
{
    /**
     * @var bool
     */
    private $value;

    private function __construct(bool $value)
    {
        $this->value = $value;
    }

    public static function fromBool(bool $value): self
    {
        return new self($value);
    }

    public function value(): bool
    {
        return $this->value;
    }
}
