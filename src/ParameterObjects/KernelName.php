<?php

declare(strict_types=1);

namespace C33s\ParameterObjects;

use C33s\ParameterObjects\Traits\StringValueObjectTrait;

final class KernelName
{
    use StringValueObjectTrait;
}
