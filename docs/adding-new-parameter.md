# Manual for adding another Parameter Object

php 7.2 or higher is required to use the maker command as it uses the
`symfony/string` component.

currently there is no maker for the Parameter Object itself, create it manually
according the existing files.

add the new Parameter Object Name like `KernelCacheDir` with its expected value to
`tests/_data/fixtures/integration/project/src/Maker/MakeIntegrationTestCommand.php`

export `set "SYMFONY_VERSION=^4.4"`

after that run the integration test suite `robo test integration` which creates
a symfony project `symfony/website-skeleton` at `tests/_output/tmp/integration/project`
referred as `project` afterwards.

in the `project`:

add the test namespaces to `composer.json`
```json
    "autoload-dev": {
        "psr-4": {
            "Tests\\Acceptance\\": "tests/acceptance/",
            "Tests\\Functional\\": "tests/functional/",
            "Tests\\Integration\\": "tests/integration/",
            "Tests\\Unit\\": "tests/unit/",
            "Tests\\Helper\\": "tests/_support/Helper/",
            "Tests\\Group\\": "tests/_support/Group/",
            "Tests\\Traits\\": "tests/_support/Traits/",
            "Tests\\Actual\\": "tests/_data/actual/",
            "Tests\\Fixtures\\": "tests/_data/DataFixtures/"
        }
    },
```

run `composer require symfony/maker-bundle`
run `composer require symfony/string`

copy everything from `tests/_data/fixtures/integration/project` to `project/src`
to have access to the `php bin/console test:make-all` command.

add run this command to regenerate all commands and tests or run it only for the
new parameter object `php bin/console make:integration-test-command KernelCacheDir`
with this command you generate

- a cest file `tests/_output/tmp/integration/project/tests/integration`
- a command `tests/_output/tmp/integration/project/src/Command/KernelCacheDirTestCommand.php`

copy the command to `tests/_data/fixtures/integration/TestCommands` and the cest
file to `tests/integration`


the templates for the generated files are located at
`tests/_data/fixtures/integration/project/src/Resources/skeleton/...`

to prevent reinit of the project use `set "SYMFONY_INTEGRATION_TEST_INITIALIZED=true"


change the `@VersionConstraint(">=4.4, <5.3")` annotation in the cest file to
reflect the symfony compatibility for this parameter.
