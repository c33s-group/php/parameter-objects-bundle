<?php

declare(strict_types=1);

// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// https://github.com/squizlabs/PHP_CodeSniffer/issues/2015
// phpcs:disable PSR1.Files.SideEffects
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace
// phpcs:disable Generic.Files.LineLength.TooLong
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClass
// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
// phpcs:disable Generic.CodeAnalysis.EmptyStatement.DetectedCatch
// phpcs:disable Generic.Formatting.SpaceBeforeCast.NoSpace
define('C33S_SKIP_LOAD_DOT_ENV', true);
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
define('C33S_ROBO_DIR', '.robo');

$roboDir = C33S_ROBO_DIR;
$previousWorkingDir = getcwd();
(is_dir($roboDir) || mkdir($roboDir)) && chdir($roboDir);
if (!is_file('composer.json')) {
    exec('composer init --no-interaction', $output, $resultCode);
    exec('composer require c33s/robofile --no-interaction', $output, $resultCode);
    exec('rm composer.yaml || rm composer.yml || return true', $output, $resultCode2);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer require c33s/robofile --no-interaction');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run --no-interaction 2>&1', $output);
    if (false === strpos(implode((array) $output), 'Nothing to install')) {
        fwrite(STDERR, "\n##### Updating .robo dependencies #####\n\n") && exec('composer install --no-interaction');
    }
}
chdir($previousWorkingDir);
require $roboDir.'/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use Consolidation\AnnotatedCommand\CommandData;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \C33s\Robo\BaseRoboFile
{
    const GLOBAL_COMPOSER_PACKAGES = [
    ];

    use \C33s\Robo\C33sTasks;
    use \C33s\Robo\C33sExtraTasks;
    // use \C33s\Robo\DebugHooksTrait;
//    use \C33s\Robo\C33sExtraTasks {
//        test as testTrait;
//    }

    protected $portsToCheck = [
//        'http' => null,
//        'mysql' => null,
    ];

    /**
     * RoboFile constructor.
     */
    public function __construct()
    {
    }

    /**
     * @hook pre-command
     */
    public function preCommand(CommandData $commandData): void
    {
//        $this->assignEnvironmentConfig($commandData);
        if ($this->isEnvironmentCi()) {
            $this->input()->setInteractive(false);
        }

        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '2.0.8',
            'php-cs-fixer' => 'v2.16.1',
            'composer-unused' => '0.7.1',
            'composer-require-checker' => '1.1.0',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init(): void
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }

        foreach (self::GLOBAL_COMPOSER_PACKAGES as $package => $version) {
            $this->composerGlobalRequire($package, $version);
        }

        $this->update();
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->phpcs($arguments);
        $this->phpcsfix($arguments);
        $this->phpstan($arguments);
        $this->composerrequirechecker($arguments);
        $this->composerunused($arguments);
    }

    public function phpcs($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/vendor/squizlabs/php_codesniffer/bin/phpcs');
    }

    public function phpcsfix($arguments = '')
    {
        $this->_execPhpQuiet("php .robo/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    public function phpstan($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/vendor/phpstan/phpstan/phpstan analyse -c phpstan.neon');
    }

    public function composerrequirechecker($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/bin/composer-require-checker.phar --ansi');
    }

    public function composerunused($arguments = '')
    {
        $this->_execPhpQuiet('php .robo/bin/composer-unused.phar');
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .robo/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Update the Project.
     */
    public function update()
    {
        if ($this->isEnvironmentCi() || $this->isEnvironmentProduction()) {
            $this->_execPhp('php ./.robo/bin/composer.phar install --no-progress --prefer-dist --optimize-autoloader --ansi');
        } else {
            $this->_execPhp('php ./.robo/bin/composer.phar install');
        }
    }
}
