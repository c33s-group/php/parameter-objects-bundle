<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelCharset;

/**
 * Parameter Object Test Command for KernelCharset
 */
class KernelCharsetTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-charset';
    private $kernelCharset;

    public function __construct(KernelCharset $kernelCharset)
    {
        $this->kernelCharset = $kernelCharset;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelCharset->value());

        return 0;
    }
}
