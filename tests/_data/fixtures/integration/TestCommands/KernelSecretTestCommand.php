<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelSecret;

/**
 * Parameter Object Test Command for KernelSecret
 */
class KernelSecretTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-secret';
    private $kernelSecret;

    public function __construct(KernelSecret $kernelSecret)
    {
        $this->kernelSecret = $kernelSecret;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelSecret->value());

        return 0;
    }
}
