<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Maker\MakeIntegrationTestCommand;
use Symfony\Component\Process\Process;

class TestMakeAllCommand extends Command
{
    protected static $defaultName = 'test:make-all';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $return = 0;
        foreach (MakeIntegrationTestCommand::PARAMETERS as $parameterObjectName => $expected) {
            $cmdReturn = $this->runConsoleCommand($parameterObjectName);
//            $name = MakeIntegrationTestCommand::buildCommandName($parameterObjectName);
//            if (!$name) {
//                ++$return;
//                continue;
//            }
//            $cmdReturn = $command->run(new ArrayInput(['po-class-name' => $parameterObjectName]), $output);
            $return += $cmdReturn;
        }

        if (0 === $return) {
            $io->success('All Classes made.');
            return $return;
        }

        $io->error('Error making classes');

        return $return;
    }

    private function runConsoleCommand($commandName): int
    {
        $process = new Process(['php', 'bin/console', 'make:integration-test-command', $commandName]);
        $process->run();
        echo $process->getOutput();
        if (!$process->isSuccessful()) {
            return 1;
        }

        return  0;
    }
}
