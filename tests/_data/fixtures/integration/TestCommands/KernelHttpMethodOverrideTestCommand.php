<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelHttpMethodOverride;

/**
 * Parameter Object Test Command for KernelHttpMethodOverride
 */
class KernelHttpMethodOverrideTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-http-method-override';
    private $kernelHttpMethodOverride;

    public function __construct(KernelHttpMethodOverride $kernelHttpMethodOverride)
    {
        $this->kernelHttpMethodOverride = $kernelHttpMethodOverride;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelHttpMethodOverride->value());

        return 0;
    }
}
