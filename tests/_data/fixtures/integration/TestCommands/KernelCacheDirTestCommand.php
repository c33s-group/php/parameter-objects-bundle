<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelCacheDir;

/**
 * Parameter Object Test Command for KernelCacheDir
 */
class KernelCacheDirTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-cache-dir';
    private $kernelCacheDir;

    public function __construct(KernelCacheDir $kernelCacheDir)
    {
        $this->kernelCacheDir = $kernelCacheDir;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelCacheDir->value());

        return 0;
    }
}
