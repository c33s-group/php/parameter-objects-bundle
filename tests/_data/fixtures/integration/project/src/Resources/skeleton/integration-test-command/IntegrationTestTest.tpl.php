<?php
// template header #########################################################################
use Symfony\Component\String\UnicodeString;

/* @var $parameter_object_name UnicodeString */
/* @var $class_name string */
/* @var $command_name string */
/* @var $expected string */
// template header end #####################################################################
?><?= "<?php\n"; ?>

namespace Tests\Integration;

use Tests\IntegrationTester;
use Codeception\Scenario;
use Tests\Integration\Annotations\VersionConstraint;

/**
 * @group integration
 * @VersionConstraint(">=4.4, <5.3")
 */
final class <?= $class_name."\n"; ?>
{
    public function <?= $parameter_object_name->prepend('test')->toString(); ?>(IntegrationTester $I, Scenario $scenario)
    {
        $I->skipIfNotInVersionRange($this);

        $I->haveInitializedIntegrationProject();
        $I->haveTheIntegrationTestCommand();

        $I->assertConsoleCommandOutputEquals('<?= $parameter_object_name->toString() ?>', '<?= $expected; ?>');
    }
}



