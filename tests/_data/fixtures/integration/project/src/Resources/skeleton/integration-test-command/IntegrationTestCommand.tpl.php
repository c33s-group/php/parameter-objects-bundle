<?php
// template header #########################################################################
use Symfony\Component\String\UnicodeString;

/* @var $parameter_object_name UnicodeString */
/* @var $command_name string */
/* @var $class_name string */
// template header end #####################################################################
?><?= "<?php\n"; ?>

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\<?= $parameter_object_name->toString(); ?>;

/**
 * Parameter Object Test Command for <?= $parameter_object_name->title(true)->toString()."\n"; ?>
 */
class <?= $class_name; ?> extends Command
{
    protected static $defaultName = '<?= $command_name; ?>';
    private <?= $parameter_object_name->camel()->prepend('$')->toString(); ?>;

    public function __construct(<?= $parameter_object_name->toString(); ?> <?= $parameter_object_name->camel()->prepend('$')->toString(); ?>)
    {
        $this-><?= $parameter_object_name->camel()->toString(); ?> = <?= $parameter_object_name->camel()->prepend('$')->toString(); ?>;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this-><?= $parameter_object_name->camel()->toString() ?>->value());

        return 0;
    }
}
