<?php

namespace App\Maker;

use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use function Symfony\Component\String\u;

final class MakeIntegrationTestCommand extends AbstractMaker
{
    const PARAMETERS = [
        'KernelCacheDir' => 'var/cache/test/',
        'KernelCharset' => 'UTF-8',
        'KernelContainerClass' => 'srcApp_KernelTestDebugContainer',
        'KernelDefaultLocale' => 'en',
        'KernelEnvironment' => 'test',
        'KernelErrorController' => 'error_controller',
        'KernelHttpMethodOverride' => '1',
        'KernelLogsDir' => 'var/log/',
        'KernelName' => 'src',
        'KernelProjectDir' => './',
        'KernelRootDir' => 'src/',
        'KernelSecret' => '$ecretf0rt3st',
        'KernelTrustedHosts' => '',
    ];

    public static function getCommandName(): string
    {
        return 'make:integration-test-command';
    }

    public static function buildCommandName($parameterObjectClassName)
    {
        $parameterObjectClassName = u($parameterObjectClassName);
        return $parameterObjectClassName->snake()->replace('_','-')->prepend('test:');
    }

    public function configureCommand(Command $command, InputConfiguration $inputConf)
    {
        $command
            ->setDescription('Creates a new console command class')
            ->addArgument('po-class-name', InputArgument::REQUIRED, 'Choose the class name to test (e.g. <fg=yellow>KernelCharset</>')
        ;
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $parameterObjectClassName = u(trim($input->getArgument('po-class-name')));

        $expected = self::PARAMETERS[$parameterObjectClassName->toString()] ?? null;

        $generator->generateClass(
            "App\\Command\\{$parameterObjectClassName->append('TestCommand')->toString()}",
            'src/Resources/skeleton/integration-test-command/IntegrationTestCommand.tpl.php',
            [
                'command_name' => $parameterObjectClassName->snake()->replace('_','-')->prepend('test:'),
                'parameter_object_name' => $parameterObjectClassName
            ]
        );
        $generator->generateClass(
            "Tests\\Integration\\{$parameterObjectClassName->append('IntegrationCest')->toString()}",
            'src/Resources/skeleton/integration-test-command/IntegrationTestTest.tpl.php',
            [
                'command_name' => self::buildCommandName($parameterObjectClassName),
                'parameter_object_name' => $parameterObjectClassName,
                'expected' => $expected
            ]
        );
        $generator->writeChanges();
        $this->writeSuccessMessage($io);
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $dependencies->addClassDependency(
            Command::class,
            'console'
        );
    }
}
