<?php

namespace Tests\Integration;

use Tests\IntegrationTester;
use Codeception\Scenario;
use Tests\Integration\Annotations\VersionConstraint;

/**
 * @group integration
 * @VersionConstraint(">=5.2")
 */
final class KernelBuildDirIntegrationCest
{
    public function testKernelBuildDir(IntegrationTester $I, Scenario $scenario)
    {
        $I->skipIfNotInVersionRange($this);
        $I->haveInitializedIntegrationProject();
        $I->assertConsoleCommandOutputEquals('KernelBuildDir', 'var/cache/test/');
    }
}



