<?php


namespace Tests\Integration\Annotations;

/**
 * @Annotation
 * @Target("ALL")
 */
final class VersionConstraint
{
    /**
     * @var string
     */
    public $value;
}
