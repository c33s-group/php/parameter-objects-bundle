<?php

namespace Tests\Integration;

use Tests\IntegrationTester;
use Codeception\Scenario;
use Tests\Integration\Annotations\VersionConstraint;

/**
 * @group integration
 * @VersionConstraint(">=4.4, <5.3")
 */
final class KernelDefaultLocaleIntegrationCest
{
    public function testKernelDefaultLocale(IntegrationTester $I, Scenario $scenario)
    {
        $I->skipIfNotInVersionRange($this);
        $I->haveInitializedIntegrationProject();
        $I->assertConsoleCommandOutputEquals('KernelDefaultLocale', 'en');
    }
}



