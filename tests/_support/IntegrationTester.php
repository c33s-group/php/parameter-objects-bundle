<?php
namespace Tests;

use Composer\Semver\Constraint\ConstraintInterface;
use Composer\Semver\Semver;
use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Webmozart\Assert\Assert;
use Codeception\Scenario;
use Composer\Semver\VersionParser;
//use function Symfony\Component\String\u;
use function Stringy\create as s;
use Tests\Integration\Annotations\VersionConstraint;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class IntegrationTester extends \Codeception\Actor
{
    /**
     * @var Filesystem
     */
    private $fs;
    /**
     * @var Finder
     */
    private $finder;
    /**
     * @var string
     */
    private $baseDir;
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var string
     */
    private $packageDir;
    /**
     * @var string
     */
    private $composerPhar;
    /**
     * @var string
     */
    private $projectDirName;
    /**
     * @var string
     */
    private $packageDirName;
    /**
     * @var bool
     */
    private static $initialized = false;
    /**
     * @var false|string
     */
    private $workingDirectory;
    /**
     * @var ConstraintInterface
     */
    private $versionConstraint;
    /**
     * @var VersionParser
     */
    private $versionParser;
    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    public function __construct(Scenario $scenario)
    {
        $this->workingDirectory = codecept_root_dir();
        parent::__construct($scenario);
        $this->fs = new Filesystem();
        $this->finder = new Finder();
        $requestedVersion = getenv('SYMFONY_VERSION');
        $this->composerPhar = getenv("COMPOSER_PHAR");
        if (false === $this->composerPhar) {
            $this->composerPhar = '.robo/bin/composer.phar';
        }
        $symfonyErrorMessage = "Test suite configuration error: the environment variable 'SYMFONY_VERSION' has to be set. For example '^4.4'";
        $composerErrorMessage ="Test suite configuration error: the environment variable 'COMPOSER_PHAR' has to be set. default '.robo/bin/composer.phar' {$this->workingDirectory}";

        Assert::string($requestedVersion, $symfonyErrorMessage);
        Assert::notEmpty($requestedVersion, $symfonyErrorMessage);
        Assert::string($this->composerPhar, $composerErrorMessage);
        Assert::notEmpty($this->composerPhar, $composerErrorMessage);
        Assert::fileExists($this->workingDirectory.$this->composerPhar, $composerErrorMessage);

        $this->composerPhar = "{$this->composerPhar} --ansi";

        $this->versionParser = new VersionParser();
        $this->versionConstraint = $this->versionParser->parseConstraints(s($requestedVersion)->toString());
        $this->baseDir = codecept_output_dir('tmp/integration');
        $this->projectDirName = 'project';
        $this->packageDirName = 'package';
        $this->projectDir = "{$this->baseDir}/{$this->projectDirName}";
        $this->packageDir = "{$this->baseDir}/{$this->packageDirName}";
        $this->annotationReader = new AnnotationReader();
    }

    use _generated\IntegrationTesterActions;


   /**
    * Define custom actions here
    */
   public function haveInitializedIntegrationProject()
    {
        if (true !== self::$initialized) {
            self::$initialized = (bool) getenv('SYMFONY_INTEGRATION_TEST_INITIALIZED');
        }


        if (!file_exists("{$this->projectDir}/composer.json")) {
            self::$initialized = false;
        } else if (!(bool) getenv('SYMFONY_INTEGRATION_TEST_FORCE_REINITIALIZE')){
            self::$initialized = true;
        }

        if (!self::$initialized) {
            $this->comment('initializing test project');
            $this->removeIntegrationTestDirectory();
            $this->createBaseDir();
            $this->createProject();
            $this->copyPackage();
            $this->configureProject();
            self::$initialized = true;
        }
    }

    public function assertPathEquals($expected, $actual)
    {
        $actualPath = $this->fs->makePathRelative($actual, $this->projectDir);
        $this->assertEquals($expected, $actualPath);
    }

    public function skipIfNotInVersionRange($object)
    {
        $reflectionClass = new ReflectionClass(get_class($object));
        $constraint = $this->annotationReader->getClassAnnotation($reflectionClass, VersionConstraint::class);
        if (null === $constraint) {
            return;
        }

        if (!Semver::satisfies($this->versionConstraint->getLowerBound()->getVersion(), $constraint->value)) {
            $this->scenario->skip("Skipping test not matching symfony version {$this->versionConstraint->getPrettyString()}");
        }
    }

    /**
     * Runs console command on project and asserts the output.
     */
    public function assertConsoleCommandOutputEquals($parameterObjectClassName, $expected, $compareCaseInsensitive = false, $resultIsPath = null)
    {
        if (!$this->fs->exists($this->getCommandDir())){
            $this->fs->mkdir($this->getCommandDir());
        }
        $this->cleanDir($this->getCommandDir());
        $this->copyTestCommandFileToProject();
//        $parameterObjectClassName = u($parameterObjectClassName);
        $parameterObjectClassName = s($parameterObjectClassName);
//        $command = $parameterObjectClassName->snake()->replace('_','-')->prepend('test:');
        $command = $parameterObjectClassName->dasherize()->prepend('test:');

        if (null === $resultIsPath) {
            $resultIsPath = false;
//             if (true === $command->containsAny('dir')) {
             if (true === $command->contains('dir')) {
                 $resultIsPath = true;
             }
        }

        $this->runConsoleCommandOnTestProject($command->toString());
        $actual = $this->grabShellOutput();
        if ($resultIsPath) {
            $this->assertPathEquals($expected, $actual);
            return;
        }

        if ($compareCaseInsensitive) {
            $this->assertEqualsIgnoringCase($expected, $actual);
            return;
        }

        $this->assertEquals($expected, $actual);
//        $this->removeTestCommandFileFromProject();
    }

    private function copyTestCommandFileToProject()
    {
        $file = $this->getTestCommandFile();
        $this->fs->copy($file->getRealPath(), "{$this->projectDir}/src/Command/{$file->getFilename()}");
    }

    private function removeTestCommandFileFromProject()
    {
        $file = $this->getTestCommandFile();
        $this->fs->remove("{$this->getCommandDir()}/{$file->getFilename()}");
    }

    private function getCommandDir(): string
    {
        return "{$this->projectDir}/src/Command";
    }

    private function getTestCommandFile(): SplFileInfo
    {
        //        $commandName = u($this->scenario->current('name'))->after('test')->append('TestCommand.php')->toString();
        $commandName = s($this->scenario->current('name'))->after('test')->append('TestCommand.php')->toString();

        return new SplFileInfo(codecept_data_dir("fixtures/integration/TestCommands/$commandName"));
    }

    private function runConsoleCommandOnTestProject($command)
    {
        $this->amInPath($this->projectDir);
        $this->comment($this->projectDir);
        $this->runShellCommand("php bin/console --env=test $command");
        $this->amInPath($this->workingDirectory);
    }

    private function copyPackage()
    {
        if (!file_exists($this->packageDir) && !mkdir($this->packageDir, 0777, true) && !is_dir($this->packageDir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $this->packageDir));
        }
        $this->finder
            ->in(getcwd())
            ->depth(0)
            ->ignoreDotFiles(true)
            ->ignoreVCS(true)
//            ->ignoreVCSIgnored(true)
            ->notPath('tests')
            ->notPath('vendor')
        ;
        foreach ($this->finder as $file) {
            if ($file->isFile()) {
                $this->fs->copy($file->getRelativePathname(), "{$this->packageDir}/{$file->getFilename()}", true);
                continue;
            }
            $this->fs->mirror($file->getRelativePathname(), "{$this->packageDir}/{$file->getFilename()}");
        }
    }

    /**
     * Deleted with IntegrationTester because it's currently the best and easiest way to completely remove a directory.
     * Double call of the `deleteDir` function to really delete it.
     */
    private function removeIntegrationTestDirectory()
    {
        $this->deleteDir($this->baseDir);
        $this->deleteDir($this->baseDir);
    }

    private function createBaseDir()
    {
        if (!file_exists($this->baseDir) && !mkdir($this->baseDir, 0777, true) && !is_dir($this->baseDir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $this->baseDir));
        }
    }

    private function configureProject()
    {
        //get package name from root
        $this->runShellCommand("php $this->composerPhar show --self --name-only");
        $packageName = $this->grabShellOutput();
        //run composer commands on project dir via
        $this->assertRegExp('@^[a-z0-9]([_.-]?[a-z0-9]+)*/[a-z0-9](([_.]?|-{0,2})[a-z0-9]+)*$@', $packageName, "Core test suite error. 'composer show --self --name-only' should return a valid package name. got '$packageName'");
        $this->runShellCommand("touch {$this->projectDir}/.no-composer-yaml");
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->projectDir} config name $packageName-integration-test");
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->projectDir} config minimum-stability dev");
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->projectDir} config prefer-stable true");
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->projectDir} config repositories.repo-integration-test path {$this->packageDir}");
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->projectDir} require $packageName symfony/flex");
//        var_dump($this->workingDirectory);
//        $this->amInPath($this->workingDirectory);
    }

    private function createProject()
    {
        $version = $this->versionConstraint->getPrettyString();
        $this->runShellCommand("php $this->composerPhar --working-dir={$this->baseDir} --no-interaction create-project symfony/website-skeleton:\"{$version}\" {$this->projectDirName}");
    }
}
