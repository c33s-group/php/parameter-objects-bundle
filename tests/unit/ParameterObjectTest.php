<?php

declare(strict_types=1);

namespace Tests\Unit;

class ParameterObjectTest extends \Codeception\Test\Unit
{
    /**
     * @dataProvider getStringClasses
     */
    public function testStringClasses(string $fullQualifiedClassName, string $value)
    {
        if ('' === $value) {
            $value = 'example value';
        }
        $object = $fullQualifiedClassName::fromString($value);

        self::assertEquals($object->value(), $value, $fullQualifiedClassName);
    }

    /**
     * @dataProvider getArrayClasses
     */
    public function testArrayClasses(string $fullQualifiedClassName, array $value)
    {
        if ([] === $value) {
            $value = ['example value', 'example value 2'];
        }
        $object = $fullQualifiedClassName::fromArray($value);

        self::assertEquals($object->value(), $value, $fullQualifiedClassName);
    }

    /**
     * @dataProvider getBoolClasses
     */
    public function testBoolClasses(string $fullQualifiedClassName, bool $value)
    {
        if ([] === $value) {
            $value = true;
        }
        $object = $fullQualifiedClassName::fromBool($value);

        self::assertEquals($object->value(), $value, $fullQualifiedClassName);
    }

    public function getStringClasses()
    {
        return [
            [\C33s\ParameterObjects\KernelCacheDir::class, ''],
            [\C33s\ParameterObjects\KernelCharset::class, ''],
            [\C33s\ParameterObjects\KernelContainerClass::class, ''],
            [\C33s\ParameterObjects\KernelDefaultLocale::class, ''],
            [\C33s\ParameterObjects\KernelEnvironment::class, ''],
            [\C33s\ParameterObjects\KernelErrorController::class, ''],
            [\C33s\ParameterObjects\KernelHttpMethodOverride::class, ''],
            [\C33s\ParameterObjects\KernelLogsDir::class, ''],
            [\C33s\ParameterObjects\KernelName::class, ''],
            [\C33s\ParameterObjects\KernelProjectDir::class, ''],
            [\C33s\ParameterObjects\KernelRootDir::class, ''],
            [\C33s\ParameterObjects\KernelSecret::class, ''],
            [\C33s\ParameterObjects\Locale::class, ''],
        ];
    }

    public function getArrayClasses()
    {
        return [
            [\C33s\ParameterObjects\KernelTrustedHosts::class, ['127.0.0.1', '192.168.0.1/32']],
        ];
    }

    public function getBoolClasses()
    {
        return [
            [\C33s\ParameterObjects\KernelDebug::class, true],
        ];
    }
}
